package com.simplonpromo2027.marecette.repository;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.simplonpromo2027.marecette.entity.Marecette;



@Repository
public class MarecetteRepository {

    @Autowired
    private DataSource dataSource;

    public List<Marecette> findAll() {
        List<Marecette> list = new ArrayList<>();
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM recette");
            ResultSet result = stmt.executeQuery();
            while (result.next()) {
               
                list.add(sqlToMarecette(result));
            }
            System.out.println("liste créée");
        } catch (SQLException e) {
            System.out.println("error from repository");
            e.printStackTrace();
        }
        return list;
    }

    private Marecette sqlToMarecette(ResultSet result)  throws SQLException{
       return new Marecette(

       result.getInt("id"),
       result.getString("contenu"),
       result.getString("title"),
       result.getString("image"),
       result.getDate("date").toLocalDate());
       

       
    }

    public Marecette findById(int id) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM recette WHERE id=?");
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            if (result.next()) {
               return sqlToMarecette(result);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Error in repository", e);
        }
        return null;
    }

    public boolean persist(Marecette marecette) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("INSERT INTO recette (contenu, title, image, date) VALUES (?, ?, ?, ?)");
            stmt.setString(1, marecette.getContenu());
            stmt.setString(2, marecette.getTitle());
            stmt.setString(3, marecette.getImage());

            if(marecette.getDate() != null) {
                stmt.setDate(3, Date.valueOf(marecette.getDate()));

            }else {
                stmt.setDate(3, null);
            }


            return stmt.executeUpdate() == 1;

        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Error in repository", e);
        }
    }

    public boolean update(Marecette marecette) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("UPDATE recette SET contenu=?, title=?, image=?, date=? WHERE id=?");
            stmt.setString(1, marecette.getContenu());
            stmt.setString(2, marecette.getTitle());
            stmt.setString(3, marecette.getImage());
            stmt.setDate(4, java.sql.Date.valueOf(marecette.getDate()));
            stmt.setInt(5, marecette.getId());
            return stmt.executeUpdate() == 1;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Error in repository", e);
        }
    }

    public boolean delete(int id) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("DELETE FROM recette WHERE id=?");
            stmt.setInt(1, id);
            return stmt.executeUpdate() == 1;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Error in repository", e);
        }
    }
}
