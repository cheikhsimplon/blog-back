package com.simplonpromo2027.marecette.repository;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.simplonpromo2027.marecette.entity.Avis;



@Repository
public class AvisRepository {

    @Autowired
    private DataSource dataSource;

    public List<Avis> findAll() {
        List<Avis> list = new ArrayList<>();
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM Avis");
            ResultSet result = stmt.executeQuery();

            while (result.next()) {
                list.add(sqlToAvis(result));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Error in repository", e);
        }
        return list;
    }

    public Avis findById(int id) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM Avis WHERE id=?");
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            if (result.next()) {
                return sqlToAvis(result);
            }

        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Error in repository", e);
        }
        return null;
    }

    private Avis sqlToAvis(ResultSet result) throws SQLException {
        return new Avis(
                result.getInt("id"),
                result.getString("description"),
                result.getDate("Date").toLocalDate());
    }

    public boolean persist(Avis Avis) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("INSERT INTO Avis (description,Date) VALUES (?,?)",
                    Statement.RETURN_GENERATED_KEYS);
            stmt.setString(1, Avis.getDescription());
            stmt.setDate(2, (Date.valueOf(Avis.getDate())));

            if (stmt.executeUpdate() == 1) {
                ResultSet keys = stmt.getGeneratedKeys();
                keys.next();
                Avis.setId(keys.getInt(1));
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Error in repository", e);
        }

        return false;
    }

    public boolean update(Avis Avis) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("UPDATE Avis SET description=? Date WHERE id=?");
            stmt.setString(1, Avis.getDescription());
            stmt.setInt(2, Avis.getId());

            if (stmt.executeUpdate() == 1) {
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Error in repository", e);
        }

        return false;
    }

    public boolean delete(int id) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("DELETE FROM Avis WHERE id=?");

            stmt.setInt(1, id);

            if (stmt.executeUpdate() == 1) {
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Error in repository", e);
        }

        return false;
    }

}
