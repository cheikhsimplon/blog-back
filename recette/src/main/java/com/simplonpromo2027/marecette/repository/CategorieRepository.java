package com.simplonpromo2027.marecette.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.simplonpromo2027.marecette.entity.Categorie;



@Repository
public class CategorieRepository  { 
    


    @Autowired
    private DataSource dataSource;

    public List<Categorie> findAll() {
        List<Categorie> list = new ArrayList<>();
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM Categorie");
            ResultSet result = stmt.executeQuery();

            while (result.next()) {
                list.add(sqlToCategorie(result));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Error in repository", e);
        }
        return list;
}


    public Categorie findById(int id) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM Categorie WHERE id=?");
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            if (result.next()) {
                return sqlToCategorie(result);
            }

        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Error in repository", e);
        }
        return null;
    }
    private Categorie sqlToCategorie(ResultSet result) throws SQLException {
        return new Categorie(
                result.getInt("id"),
                result.getString("name"));
                
    }

 public boolean persist(Categorie Categorie) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("INSERT INTO Categorie (name) VALUES (?)", Statement.RETURN_GENERATED_KEYS);
            stmt.setString(1, Categorie.getName());
            
            

            if(stmt.executeUpdate() == 1) {
                ResultSet keys = stmt.getGeneratedKeys();
                keys.next();
                Categorie.setId(keys.getInt(1));
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Error in repository", e);
        }

        return false;
    }

    public boolean update(Categorie categorie) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("UPDATE Categorie SET name=? WHERE id=?");
            stmt.setString(1, categorie.getName());
            stmt.setInt(2, categorie.getId());

            
            
            if(stmt.executeUpdate() == 1) {
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Error in repository", e);
        }

        return false;
    }

        

       
       
                
                 public boolean delete(int id) {
                    try (Connection connection = dataSource.getConnection()) {
                        PreparedStatement stmt = connection.prepareStatement("DELETE FROM dog WHERE id=?");
            
                        
                        stmt.setInt(1, id);
            
                        if(stmt.executeUpdate() == 1) {
                            return true;
                        }
                    } catch (SQLException e) {
                        e.printStackTrace();
                        throw new RuntimeException("Error in repository", e);
                    }
            
                    return false;
                }
            
            
          }  
    
    
    
    
