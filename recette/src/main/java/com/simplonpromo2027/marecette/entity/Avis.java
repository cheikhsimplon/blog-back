package com.simplonpromo2027.marecette.entity;

import java.time.LocalDate;

public class Avis {

private Integer id;
private String Description;
private LocalDate Date;


public Avis(String description, LocalDate date) {
    Description = description;
    Date = date;
}


public Avis(Integer id, String description, LocalDate date) {
    this.id = id;
    Description = description;
    Date = date;
}


public Avis() {
}


public Integer getId() {
    return id;
}


public void setId(Integer id) {
    this.id = id;
}


public String getDescription() {
    return Description;
}


public void setDescription(String description) {
    Description = description;
}


public LocalDate getDate() {
    return Date;
}


public void setDate(LocalDate date) {
    Date = date;
}



}
