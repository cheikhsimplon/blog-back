package com.simplonpromo2027.marecette.entity;

import java.time.LocalDate;

public class Marecette {

private Integer id;

private String Contenu ;
private String Title ;
private String Image;
private LocalDate Date;




public Marecette() {
}

public Marecette(String contenu, String title, String image, LocalDate date) {
    this.Contenu = contenu;
    this.Title = title;
    this.Image = image;
    this.Date= date;
}
public Marecette(Integer id, String contenu, String title, String image, LocalDate date) {
    this.id = id;
    Contenu = contenu;
    Title = title;
    Image = image;
    Date = date;
}
public Integer getId() {
    return id;
}
public void setId(Integer id) {
    this.id = id;
}

public String getContenu() {
    return Contenu;
}
public void setContenu(String contenu) {
    Contenu = contenu;
}
public String getTitle() {
    return Title;
}
public void setTitle(String title) {
    Title = title;
}
public String getImage() {
    return Image;
}
public void setImage(String image) {
    Image = image;
}
public LocalDate getDate() {
    return Date;
}
public void setDate(LocalDate date) {
    Date = date;
}
}

