package com.simplonpromo2027.marecette.entity;


public class Categorie {

    private Integer Id;
    private String name;
    
    public Categorie() {
    }

    public Categorie(String name) {
        this.name = name;
    }

    public Categorie(Integer id, String name) {
        Id = id;
        this.name = name;
    }

    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}