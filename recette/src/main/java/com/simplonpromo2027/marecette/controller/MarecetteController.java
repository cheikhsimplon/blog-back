package com.simplonpromo2027.marecette.controller;
import java.sql.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.simplonpromo2027.marecette.entity.Marecette;
import com.simplonpromo2027.marecette.repository.MarecetteRepository;

import jakarta.validation.Valid;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/Marecette") 
public class MarecetteController {

    @Autowired
    private MarecetteRepository MarecetteRepo;

    @GetMapping
    public List<Marecette> all() {
        return MarecetteRepo.findAll();
    }

    @GetMapping("/{id}")
    public Marecette one(@PathVariable int id) {
        Marecette Marecette = MarecetteRepo.findById(id);
        if(Marecette == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        return Marecette;
    }
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Marecette add( @Valid @RequestBody Marecette Marecette) {
        MarecetteRepo.persist(Marecette);
        return Marecette;
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void remove(@PathVariable int id) {
        one(id);  
        MarecetteRepo.delete(id);
    }

    @PutMapping("/{id}")
    public Marecette replace(@PathVariable int id, @Valid @RequestBody Marecette Marecette) {
        one(id); 
        Marecette.setId(id); 
        MarecetteRepo.update(Marecette);
        return Marecette;
    }
}



    
