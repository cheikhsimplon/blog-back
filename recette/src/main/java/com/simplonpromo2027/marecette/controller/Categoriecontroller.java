package com.simplonpromo2027.marecette.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.simplonpromo2027.marecette.entity.Categorie;
import com.simplonpromo2027.marecette.repository.CategorieRepository;

public class Categoriecontroller {
    @RestController
    @CrossOrigin("*")
    @RequestMapping("/api/Categorie") 
    public class CategorieController {
    
        @Autowired
        private CategorieRepository CategorieRepo;
    
        @GetMapping
        public List<Categorie> all() {
            return CategorieRepo.findAll();
        }
    
        @GetMapping("/{id}")
        public Categorie one(@PathVariable int id) {
            Categorie Categorie = CategorieRepo.findById(id);
            if(Categorie == null) {
                throw new ResponseStatusException(HttpStatus.NOT_FOUND);
            }
            return Categorie;
        }
    }
}
    
