package com.simplonpromo2027.marecette.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.simplonpromo2027.marecette.entity.Avis;
import com.simplonpromo2027.marecette.repository.AvisRepository;

public class Aviscontroller {
    
        @RestController
        @RequestMapping("/api/Avis") 
        public class AvisController {
        
            @Autowired
            private AvisRepository AvisRepo;
        
            @GetMapping
            public List<Avis> all() {
                return AvisRepo.findAll();
            }
        
            @GetMapping("/{id}")
            public Avis one(@PathVariable int id) {
                Avis Avis = AvisRepo.findById(id);
                if(Avis == null) {
                    throw new ResponseStatusException(HttpStatus.NOT_FOUND);
                }
                return Avis;
            }
        }
    }

