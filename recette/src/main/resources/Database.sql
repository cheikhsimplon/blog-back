-- Active: 1709546088749@@127.0.0.1@3306@recette

DROP TABLE if EXISTS categorie ;
DROP TABLE if exists avis;
DROP TABLE IF EXISTS recette;

drop TABLE if exists user ;

CREATE TABLE recette (
     id INT PRIMARY KEY AUTO_INCREMENT,
  Date DATE,
    contenu VARCHAR (5000) NOT null,
    title  varchar (64) not NULL,
    image VARCHAR (224) not NULL
);


CREATE Table avis (
  id INT PRIMARY KEY AUTO_INCREMENT,
  description VARCHAR (225) not NULL,
  DATE DATETIME,
  id_recette int,
  Foreign Key (id_recette) REFERENCES recette(id) ON DELETE CASCADE
);

CREATE Table categorie (
  id Int PRIMARY key AUTO_INCREMENT,
  name VARCHAR (64) not NULL,
  id_avis int not NULL,
   Foreign Key (id_avis) REFERENCES avis(id) ON DELETE CASCADE
   );

   CREATE TABLE user(
id INT PRIMARY KEY AUTO_INCREMENT,
    email VARCHAR(255) NOT NULL UNIQUE, 
    password VARCHAR(255) NOT NULL,
    role VARCHAR(64) NOT NULL
);
 


INSERT INTO recette (Date,contenu,title,image) VALUES
('2023-08-10','Faites chauffer une grande casserole d’eau salée , plongez-y vos pâtes le moment venu.Ensuite vous decoupez les girolles,Dans une sauteuse, faites revenir une gousse d’ail dans un filet d’huile d’olive puis ajoutez le peperoncino, les tiges de persil et la branche de romarin pour le soffritto.

Ajoutez ensuite les girolles. Faites cuire à feu vif quelques minutes puis déglacez avec du vin blanc. Baissez le feu et laissez mijoter.

Ajoutez une ou deux louches d’eau de cuisson des pâtes à votre sauce.

Quand les fettuccine sont al dente, plongez-les dans la sauce.

Ajoutez une belle noisette de beurre et du parmesan.

Dressez immédiatement dans chaque assiette avec du persil frais finement haché.','fettucine aux girolles','https://i.pinimg.com/564x/d3/89/da/d389da187fbab18c111da352fb3b94fe.jpg'),


('2023-09-11','Faites chauffer une grande casserole d’eau salée et plongez-y les rigatoni. 

Pendant ce temps, dans une sauteuse, faites revenir une gousse d’ail et quelques tiges de basilic dans de l’huile d’olive. 

Ajoutez ensuite les tomates cerises jaunes (vous verrez quel parfum…!) et écrasez-les délicatement. Faites mijoter 5 min. 

Ajoutez ensuite quelques filets de thon (la moitié du bocal environ), écrasez-les puis faites mijoter 2/3 minutes. Enlevez la gousse d’ail et les tiges de basilic. 

Quand vos rigatoni sont al dente, plongez-les dans votre sauce. 

Ajoutez un peu de basilic frais ciselé & mélangez. 

Servez dans une jolie assiette avec du poivre et le zeste de citron vert.','rigatoni au thon','https://i.pinimg.com/564x/08/ca/2c/08ca2c676f5359ec941e9193d0558c53.jpg'),

('2023_10_12','Epluchez les pommes de terres et coupez les en petits dès de plus ou moins 1 cm.

Dans une casserole avec un filet d’huile d’olive, faites revenir un bon soffritto (oignon, carottes et branche de céleri émincées).

Ajoutez la tranche de pancetta ou guanciale et la branche de romarin au soffritto et faites revenir quelques minutes.

Ajoutez les pommes de terres. Faites revenir quelques minutes.

Enlevez la branche de romarin. Recouvrez ensuite d’eau et ajoutez la croute de parmesan.

Quand les pommes de terres sont tendres, ajoutez les pâtes.

Quand les pâtes sont cuites, ajoutez une dose généreuse de parmesan râpé, et le provolone coupé en petits dès.

Servez dans une assiette creuse ou un bol avec du poivre fraichement moulu et un peu de romarin','pasta e patate','https://i.pinimg.com/564x/e1/60/89/e16089cb3c31e3dbafa9e2b1b6658ee1.jpg'),

('2023_11_14','Préparez les côtelettes en grattant au couteau la fine peau qui recouvre l’os pour la retirer, et découpez les nerfs superflus pour former un morceau de viande bien rond. Battez la viande pour lui donner une épaisseur uniforme.

Passez à la panure : passez chaque côtelette une première fois rapidement dans la chapelure, puis dans les oeufs battus, puis dans la chapelure. Appyuez bien sur la viande de chaque côté, puis répétez une deuxième fois le passage dans l’oeuf et la chapelure. Cela donnera une panure épaisse et bien croquante.

Faites fondre le beurre clarifié dans votre poêle. Quand il est chaud, cuisez vos côtelettes une à une environ 4 minutes de chaque côté. (Le temps de cuisson dépendra de l’épaisseur de vos morceaux de viande) Versez un peu de beurre clarifié avec une cuillère sur l’os pour lui donner une couleur dorée et faire disparaître les petits nerfs rouges. Quand les côtelettes seront cuites, la viande sera tendre et un peu rosée à l’intérieur.

Servez dans une assiette avec la roquette, quelques tranches de citron et un peu de fleur de sel.','côtelettes milanaises','https://i.pinimg.com/564x/00/52/95/0052956d357b6ee0b42a2a56940ef2be.jpg'),

('2023_11_16','Emincez l’oignon et découpez le brocoli.

Dans une grande casserole d’eau salée, faites cuire le brocoli 10 à 15 min.

Dans une poêle, faites revenir les échalotes puis ajoutez les raisins secs, les pignons de pin et le safran avec son eau aromatisée.

Ajoutez ensuite le brocoli que vous écraserez pour obtenir la sauce.

Dans l’eau de cuisson du brocoli, cuisez vos pâtes.

Une fois qu’elles sont al dente, ajoutez-les à la sauce.

Mélangez bien puis dressez dans chaque assiette avec un peu de chapelure que vous aurez toasté pour donner un peu de croquant à la recette.','pates aux brocolis','https://i.pinimg.com/564x/fa/7a/e7/fa7ae758cbcbef60dbef5d94e18098b4.jpg'),

('2023_12_19','Retirez la couenne du guanciale et découpez-le en petits morceaux carré d’environ 1cm.

Râper le pecorino et le conserver dans un bol. Concasser le poivre et le réserver.

Plongez les spaghetti ou les rigatoni dans une grande casserole d’eau bouillante (pas besoin de saler l’eau – le pecorino et le guanciale se chargeront d’apporter le seul nécessaire à cette recette!).

Faites revenir le guanciale dans une poêle sans matière grasses à feu vif jusqu’à ce qu’il devienne bien croquant. Laissez ensuite cuire à feu doux jusqu’à ce que le gras deviennent bien transparent.

Dans un saladier, verser l’oeuf, les jaunes d’œufs, les trois quarts du pecorino et le poivre et fouettez jusqu’à obtenir une crème onctueuse.

Lorsque les pâtes sont cuites, conservez 2/3 louches d’eau de cuisson, égouttez-les et versez-les directement dans le saladier.

Mélangez bien en ajoutant un peu d’eau de cuisson si nécessaire.

Ajoutez ensuite le guanciale et une cuillère à soupe du gras de cuisson. Mélangez à nouveau.

Servez immédiatement avec quelques tours de poivre du moulin en plus et du pecorino.','spaghetti alla carbonnara','https://i.pinimg.com/564x/73/a1/67/73a167c94396498df5fdaa1bd90e408b.jpg');





INSERT INTO avis (description, date, id_recette)VALUES
('Très jolies aux cantucci aux amendes bien mis en valeur dans vos photos.','2024-03-24', 1),
('Ils sont bien beau tes crepes toscanes ','2024-03-25', 1),
('ils sont bons et adorable la saveur du glace au yaourt! Bisous','2024-03-26', 1),
('Sympa au petit déjeuner. Tes photos les mettent bien en valeur bises','2024-03-27', 2);



INSERT INTO categorie (name , id_avis) VALUES
('primi', 1),
('végétarien', 1),
('dolci', 1),
('Aperitivo et antipasti', 1),
('contorni', 1),
('soupes et minestroni', 1),
('pain,pizza et foccacia', 1),
('secondi', 1);




INSERT INTO user (email,password,role) VALUES 
('admin@test.com', '$2y$10$ExbjqPDMeGm1lZ6GiaDgO.YZqvDucH5TxYjrK2jo1JxI3V0RbHwvq', 'ROLE_ADMIN'),
('cheikh@test.com', '$2y$10$ExbjqPDMeGm1lZ6GiaDgO.YZqvDucH5TxYjrK2jo1JxI3V0RbHwvq', 'ROLE_USER');



     